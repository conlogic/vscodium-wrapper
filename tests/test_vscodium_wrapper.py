# ============================================================================
# Title          : Wrapper for VSCodium to use multiple profiles
#
# Classification : Python module
#
# Author         : Dirk Ullrich
#
# Date           : 2021-11-18
#
# Description    : See documentation string below.
# ============================================================================

'''
Unit tests for module :py:mod:`vscodium_wrapper`.
'''


import unittest
import tempfile
import pathlib
import shutil

import vscodium_wrapper


class base_vscodium_wrapper_TC(unittest.TestCase):
    '''
    Base class for :py:mod:`vscodium_wrapper` tests.
    '''
    def setUp(self):
        # Path for the base test directory.
        self.test_base_dpath = \
            pathlib.Path(tempfile.gettempdir()) / 'vc-tests'
        # Standard test configuration parameters.
        self.test_config_params = vscodium_wrapper.CONF_PARAMS.copy()
        self.test_config_params['home_dir'] = self.test_base_dpath

        # Always start with a clean test base directory.
        if self.test_base_dpath.is_dir():
            shutil.rmtree(self.test_base_dpath)
        self.test_base_dpath.mkdir(parents=True)


    def prep_act_profile(self, config_params, act_profile=''):
        '''
        Prepare a file to store the active VSCodium profile with profile name
        `act_profile` for configuration parameters `config_params`.

        :return: path of the prepared file.
        '''
        home_dpath = config_params.get('home_dir', self.test_base_dpath)
        act_prof_fpath = home_dpath / config_params['act_profile_file']
        act_prof_dpath = act_prof_fpath.parent
        act_prof_dpath.mkdir(parents=True, exist_ok=True)
        with open(act_prof_fpath, 'wt') as act_prof_fobj:
            act_prof_fobj.write(f"{act_profile}\n")

        return act_prof_fpath


    def prep_profile(self, config_params, profile, what=['data', 'exts']):
        '''
        Prepare the user data directory (if ``'data'`` is an element of
        `what`) and / or the extensions directory (if ``'exts'`` is an element
        of `what`) for profile named `profile` using configuration parameters
        `config_params`.
        '''
        home_dpath = config_params.get('home_dir', self.test_base_dpath)
        if 'data' in what:
            data_rdpath = config_params['user_data_base']
            data_profile_dpath = home_dpath / data_rdpath / profile
            data_profile_dpath.mkdir(exist_ok=True, parents=True)
        if 'exts' in what:
            exts_rdpath = config_params['extension_base']
            exts_profile_dpath = home_dpath / exts_rdpath / profile
            exts_profile_dpath.mkdir(exist_ok=True, parents=True)


class calc_all_profiles_TC(base_vscodium_wrapper_TC):
    '''
    Tests for function :py:func:`calc_all_profiles`.
    '''


    def test_data_no_dir(self):
        '''
        Test with an existing user data non-directory.
        '''
        data_rdpath = 'data'
        config_params = self.test_config_params.copy()
        config_params['user_data_base'] = data_rdpath
        data_path = self.test_base_dpath / data_rdpath
        data_path.touch()
        with self.assertRaises(vscodium_wrapper.VSCodiumError):
            vscodium_wrapper.calc_all_profiles(config_params)


    def test_exts_no_dir(self):
        '''
        Test with an existing extensions non-directory.
        '''
        exts_rdpath = 'exts'
        config_params = self.test_config_params.copy()
        config_params['extension_base'] = exts_rdpath
        exts_path = self.test_base_dpath / exts_rdpath
        exts_path.touch()
        with self.assertRaises(vscodium_wrapper.VSCodiumError):
            vscodium_wrapper.calc_all_profiles(config_params)


    def test_inconsistent_profiles(self):
        '''
        Test with inconsistent profiles.
        '''
        self.prep_profile(self.test_config_params, 'foo')
        self.prep_profile(self.test_config_params, 'bar', what=['data'])
        self.prep_profile(self.test_config_params, 'baz', what=['exts'])
        with self.assertRaises(vscodium_wrapper.VSCodiumError):
            vscodium_wrapper.calc_all_profiles(self.test_config_params)


    def test_no_data_dir(self):
        '''
        Test with non-existing user data directory and empty extensions
        directory.
        '''
        exts_rdpath = self.test_config_params['extension_base']
        exts_dpath = self.test_base_dpath / exts_rdpath
        exts_dpath.mkdir(parents=True)
        res_profiles = vscodium_wrapper.calc_all_profiles(
            self.test_config_params)
        self.assertSetEqual(res_profiles, set([]))


    def test_no_exts_dir(self):
        '''
        Test with empty user data directory and non-existing extensions
        directory.
        '''
        data_rdpath = self.test_config_params['user_data_base']
        data_dpath = self.test_base_dpath / data_rdpath
        data_dpath.mkdir(parents=True)
        res_profiles = vscodium_wrapper.calc_all_profiles(
            self.test_config_params)
        self.assertSetEqual(res_profiles, set([]))


    def test_empty_dirs(self):
        '''
        Test with empty user data directory and empty extensions directory.
        '''
        data_rdpath = self.test_config_params['user_data_base']
        data_dpath = self.test_base_dpath / data_rdpath
        data_dpath.mkdir(parents=True)
        exts_rdpath = self.test_config_params['extension_base']
        exts_dpath = self.test_base_dpath / exts_rdpath
        exts_dpath.mkdir(parents=True)
        res_profiles = vscodium_wrapper.calc_all_profiles(
            self.test_config_params)
        self.assertSetEqual(res_profiles, set([]))


    def test_ex_profiles(self):
        '''
        Test with existing profiles.
        '''
        profiles = {'foo', 'bar'}
        for profile in profiles:
            self.prep_profile(self.test_config_params, profile)
        res_profiles = vscodium_wrapper.calc_all_profiles(
            self.test_config_params)
        self.assertSetEqual(res_profiles, profiles)


class read_active_profile_TC(base_vscodium_wrapper_TC):
    '''
    Tests for function :py:func:`read_active_profile`.
    '''


    def test_noex_fpath_no_fallback(self):
        '''
        Test using a path without existing file and no fallback.
        '''
        nonex_fname = 'foo'
        config_params = self.test_config_params.copy()
        config_params['act_profile_file'] = nonex_fname
        nonex_fpath = self.test_base_dpath / nonex_fname
        if nonex_fpath.is_file():
            nonex_fpath.unlink()
        with self.assertRaises(vscodium_wrapper.VSCodiumError):
            vscodium_wrapper.read_active_profile(
                config_params, fallback=False)


    def test_empty_file_no_fallback(self):
        '''
        Test using an empty file and no fallback.
        '''
        config_params = self.test_config_params.copy()
        self.prep_act_profile(config_params)
        with self.assertRaises(vscodium_wrapper.VSCodiumError):
            vscodium_wrapper.read_active_profile(
                config_params, fallback=False)


    def test_missg_param_fallback(self):
        '''
        Test with missing configuration parameter and fallback.
        '''
        config_params = self.test_config_params.copy()
        del config_params['act_profile_file']
        with self.assertRaises(vscodium_wrapper.VSCodiumError):
            vscodium_wrapper.read_active_profile(
                config_params, fallback=True)


    def test_noex_fpath_fallback(self):
        '''
        Test using a path without existing file and with fallback.
        '''
        nonex_fname = 'foo'
        config_params = self.test_config_params.copy()
        config_params['act_profile_file'] = nonex_fname
        nonex_fpath = self.test_base_dpath / nonex_fname
        if nonex_fpath.is_file():
            nonex_fpath.unlink()
        exp_profile = config_params['base_profile']
        res_profile = vscodium_wrapper.read_active_profile(
            config_params, fallback=True)
        self.assertSequenceEqual(res_profile, exp_profile)


    def test_empty_file_fallback(self):
        '''
        Test using an empty file and with fallback.
        '''
        config_params = self.test_config_params.copy()
        self.prep_act_profile(config_params)
        exp_profile = config_params['base_profile']
        res_profile = vscodium_wrapper.read_active_profile(
            config_params, fallback=True)
        self.assertSequenceEqual(res_profile, exp_profile)


    def test_nonempty_file_no_fallback(self):
        '''
        Test using an non-empty file and no fallback.
        '''
        act_profile = 'foo'
        config_params = self.test_config_params.copy()
        self.prep_act_profile(config_params, act_profile)
        res_profile = vscodium_wrapper.read_active_profile(
            config_params, fallback=False)
        self.assertSequenceEqual(res_profile, act_profile)


class write_active_profile(base_vscodium_wrapper_TC):
    '''
    Tests for function :py:func:`write_active_profile`.
    '''
    def check_act_profile(self, exp_profile=None, config_params=None):
        '''
        Test that the file that stores the name of the active profile stores
        name `exp_profile`, where for a ``None`` value the base profile is
        used.

        A not-``None`` value of `config_params` is used as configuration
        parameters instead of the default one of tests.
        '''
        if config_params is None:
            config_params = self.test_config_params

        if exp_profile is None:
            exp_profile = config_params['base_profile']

        home_dpath = config_params.get('home_dir', self.test_base_dpath)
        act_prof_fpath = home_dpath / config_params['act_profile_file']
        with open(act_prof_fpath, 'rt') as act_prof_fobj:
            res_profile = act_prof_fobj.readline().strip()

        self.assertSequenceEqual(res_profile, exp_profile)


    def test_missg_param(self):
        '''
        Test using a configuration that misses the respective parameter.
        '''
        config_params = self.test_config_params.copy()
        del config_params['base_profile']
        with self.assertRaises(vscodium_wrapper.VSCodiumError):
            vscodium_wrapper.write_active_profile(config_params, 'foo')


    def test_noex_store_file_nobase(self):
        '''
        Test with non-existing store file and active non-base profile.
        '''
        profile = 'foo'
        is_written = vscodium_wrapper.write_active_profile(
            self.test_config_params, profile)
        self.assertTrue(is_written)
        self.check_act_profile(profile)


    def test_noex_store_file_base(self):
        '''
        Test with non-existing store file and active base profile.
        '''
        is_written = vscodium_wrapper.write_active_profile(
            self.test_config_params)
        self.assertTrue(is_written)
        self.check_act_profile()


    def test_ex_store_file_ne_profile(self):
        '''
        Test with existing store file and different active base profile.
        '''
        profile = 'foo'
        self.prep_act_profile(self.test_config_params, profile)
        is_written = vscodium_wrapper.write_active_profile(
            self.test_config_params)
        self.assertTrue(is_written)
        self.check_act_profile()


    def test_ex_store_file_eq_profile(self):
        '''
        Test with existing store file and same active base profile.
        '''
        profile = 'foo'
        self.prep_act_profile(self.test_config_params, profile)
        is_written = vscodium_wrapper.write_active_profile(
            self.test_config_params, profile)
        self.assertFalse(is_written)
        self.check_act_profile(profile)


class calc_active_profile_file_TC(base_vscodium_wrapper_TC):
    '''
    Tests for function :py:func:`calc_active_profile_file`.
    '''


    def test_missg_param(self):
        '''
        Test using a configuration that misses the respective parameter.
        '''
        config_params = self.test_config_params.copy()
        del config_params['act_profile_file']
        with self.assertRaises(vscodium_wrapper.VSCodiumError):
            vscodium_wrapper.calc_active_profile_file(config_params)


    def test_std_file(self):
        '''
        Test using the standard active profile file.
        '''
        config_params = self.test_config_params.copy()
        exp_fname = config_params['act_profile_file']
        exp_fpath = self.test_base_dpath / exp_fname
        res_fpath = vscodium_wrapper.calc_active_profile_file(config_params)
        self.assertEqual(res_fpath, exp_fpath)


    def test_alt_file(self):
        '''
        Test using an alternative active profile file.
        '''
        alt_fname = 'foo'
        config_params = self.test_config_params.copy()
        config_params['act_profile_file'] = alt_fname
        exp_fpath = self.test_base_dpath / alt_fname
        res_fpath = vscodium_wrapper.calc_active_profile_file(config_params)
        self.assertEqual(res_fpath, exp_fpath)


class calc_base_profile_TC(base_vscodium_wrapper_TC):
    '''
    Tests for function :py:func:`calc_base_profile`.
    '''


    def test_missg_param(self):
        '''
        Test using a configuration that misses the respective parameter.
        '''
        config_params = self.test_config_params.copy()
        del config_params['base_profile']
        with self.assertRaises(vscodium_wrapper.VSCodiumError):
            vscodium_wrapper.calc_base_profile(config_params)


    def test_std_profile(self):
        '''
        Test for the standard profile name.
        '''
        exp_profile = self.test_config_params['base_profile']
        res_profile = vscodium_wrapper.calc_base_profile(
            self.test_config_params)
        self.assertEqual(res_profile, exp_profile)


class calc_user_data_dir_TC(base_vscodium_wrapper_TC):
    '''
    Tests for function :py:func:`calc_user_data_dir`.
    '''


    def test_missg_param(self):
        '''
        Test using a configuration that misses the respective parameter.
        '''
        config_params = self.test_config_params.copy()
        del config_params['user_data_base']
        with self.assertRaises(vscodium_wrapper.VSCodiumError):
            vscodium_wrapper.calc_user_data_dir(config_params)


    def test_std_dir(self):
        '''
        Test for the standard directory path.
        '''
        exp_rdpath = self.test_config_params['user_data_base']
        exp_dpath = self.test_base_dpath / exp_rdpath
        res_dpath = vscodium_wrapper.calc_user_data_dir(
            self.test_config_params)
        self.assertEqual(res_dpath, exp_dpath)


class calc_extensions_dir_TC(base_vscodium_wrapper_TC):
    '''
    Tests for function :py:func:`calc_extensions_dir`.
    '''


    def test_missg_param(self):
        '''
        Test using a configuration that misses the respective parameter.
        '''
        config_params = self.test_config_params.copy()
        del config_params['extension_base']
        with self.assertRaises(vscodium_wrapper.VSCodiumError):
            vscodium_wrapper.calc_extensions_dir(config_params)


    def test_std_dir(self):
        '''
        Test for the standard directory path.
        '''
        exp_rdpath = self.test_config_params['extension_base']
        exp_dpath = self.test_base_dpath / exp_rdpath
        res_dpath = vscodium_wrapper.calc_extensions_dir(
            self.test_config_params)
        self.assertEqual(res_dpath, exp_dpath)


class calc_home_dir_TC(base_vscodium_wrapper_TC):
    '''
    Tests for function :py:func:`calc_home_dir`
    '''


    def test_noex_dpath(self):
        '''
        Test using a path without an existing directory.
        '''
        config_params = self.test_config_params.copy()
        config_params['home_dir'] = 'foo'
        with self.assertRaises(vscodium_wrapper.VSCodiumError):
            vscodium_wrapper.calc_home_dir(config_params)


    def test_std_home_dir(self):
        '''
        Test using the standard home directory.
        '''
        config_params = self.test_config_params.copy()
        del config_params['home_dir']
        exp_dpath = pathlib.Path.home()
        res_dpath = vscodium_wrapper.calc_home_dir(config_params)
        self.assertEqual(res_dpath, exp_dpath)


    def test_alt_dpath(self):
        '''
        Test using an alternative path for an existing directory.
        '''
        exp_dpath = self.test_base_dpath
        res_dpath = vscodium_wrapper.calc_home_dir(self.test_config_params)
        self.assertEqual(res_dpath, exp_dpath)


    def test_alt_str_dpath(self):
        '''
        Test using an alternative path given as string for an existing
        directory.
        '''
        config_params = self.test_config_params.copy()
        config_params['home_dir'] = str(self.test_base_dpath)
        exp_dpath = self.test_base_dpath
        res_dpath = vscodium_wrapper.calc_home_dir(config_params)
        self.assertEqual(res_dpath, exp_dpath)


# ----------------------------------------------------------------------------
# Local Variables:
# ispell-local-dictionary: "en"
# End:
