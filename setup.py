# ============================================================================
# Title          : Wrapper for VSCodium to use multiple profiles
#
# Classification : Python module
#
# Author         : Dirk Ullrich
#
# Date           : 2021-11-11
#
# Description    : See documentation string below.
# ============================================================================

'''
Setup script.
'''


import re
import pathlib
from setuptools import setup


def extract_pkg_info(fpath_readme):
    '''
    Extract from the reST read me file with path `fpath_readme` of a Python
    distribution package its distribution name, its short and its long
    description.

    :return: a triple with the package's distribution name, its short and its
    long description as components.
    '''
    # Regexp to match a line beginning a comment.
    comment_beg_line_rx = r'^\.\.\s$'
    # Regexp to match a title overline / underline.
    title_delim_line_rx = r'^=+\s*$'
    # Regexp to match the title line.
    title_rx = r'^\s*``(.*)``\s*:\s*(.*)$'

    state = 'header'
    dist_name = None
    short_description = None
    long_description = ''

    with open(fpath_readme, 'rt') as fobj_readme:
        for l in fobj_readme:
            if state == 'header':
                if re.match(title_delim_line_rx, l) is not None:
                    state = 'title'
            elif state == 'title':
                l_no_nl = l.rstrip()
                m_title = re.match(title_rx, l_no_nl)
                if m_title is not None:
                    dist_name = m_title.group(1)
                    short_description = m_title.group(2)
                elif re.match(title_delim_line_rx, l) is not None:
                    state = 'body_text'
            elif state == 'body_text':
                if re.match(comment_beg_line_rx, l) is not None:
                    break

            if not state == 'header':
                long_description += l

    # Strip trailing empty lines.
    long_description = long_description.rstrip('\n')
    long_description += '\n'

    return (dist_name, short_description, long_description)


# We need the absolute path for the read me file, to get, for instance, the
# short description from elsewhere.
this_dpath = pathlib.Path(__file__).parent
readme_fpath = this_dpath / 'README.rst'
(dist_pkg, short_desc, long_desc) = extract_pkg_info(readme_fpath)


if __name__ == '__main__':
    # The extraction mechanism for attributes in ``setup.cfg`` seems not to
    # work for attributes of this very module.  Therefore we pass the info
    # extracted from the read me file by the :py:func:`extract_descriptions`
    # function above as arguments to the :py:func:`setup` function here.
    setup(
        name=dist_pkg,
        description=short_desc,
        long_description=long_desc
    )


# ----------------------------------------------------------------------------
# Local Variables:
# ispell-local-dictionary: "en"
# End:
