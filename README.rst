..
 =============================================================================
 Title          : Wrapper for VSCodium to use multiple profiles

 Classification : reST text file

 Author         : Dirk Ullrich

 Date           : 2021-11-16

 Description    : Read me file for the package.
 =============================================================================

===================================================================
``vscodium-wrapper``: Wrapper for VSCodium to use multiple profiles
===================================================================

A wrapper for the VSCodium command is implemented to manage different usage
profiles.  One of it, the so-called base profile, is the base of all other
ones, i.e. the non-base usage profiles are built on top of the base profile.
Exactly one usage profile is chosen to be the active one.  This active profile
is used for the next run of VSCodium.


To manage these usage profiles, additional command line options are used:

# ``-L`` / ``--list-profiles``: list all profiles, and mark the active one.

+ ``-S`` `profile`  / ``--set-profile`` `profile`: set profile with name
  `profile` as the active one.


..
 -----------------------------------------------------------------------------
 Local Variables:
 mode: rst
 ispell-local-dictionary: "en"
 End:

..  LocalWords:  VSCodium
