..
 =============================================================================
 Title          : Wrapper for VSCodium to use multiple profiles

 Classification : reST text file

 Author         : Dirk Ullrich

 Date           : 2021-11-16

 Description    : Master file for the documentation.
 =============================================================================


==============================================================================
Documentation for ``vscodium-wrapper``
==============================================================================

.. toctree::
   :maxdepth: 2

   overview

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`


..
 -----------------------------------------------------------------------------
 Local Variables:
 mode: rst
 ispell-local-dictionary: "en"
 End:
