..
 =============================================================================
 Title          : Wrapper for VSCodium to use multiple profiles

 Classification : reST text file

 Author         : Dirk Ullrich

 Date           : 2021-11-11

 Description    : Package overview.
 =============================================================================


===================================
General information for the package
===================================

.. toctree::
   :maxdepth: 1

   readme

   license

   changelog


..
 -----------------------------------------------------------------------------
 Local Variables:
 mode: rst
 ispell-local-dictionary: "en"
 End:
