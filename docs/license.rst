..
 =============================================================================
 Title          : Wrapper for VSCodium to use multiple profiles

 Classification : reST text file

 Author         : Dirk Ullrich

 Date           : 2021-11-11

 Description    : License for the package.
 =============================================================================

License
=======

This package is licensed by the LGPL 3.  Fur the full text of this license see
file :download:`LICENSE.txt <../LICENSE.txt>` within this package.




..
 -----------------------------------------------------------------------------
 Local Variables:
 mode: rst
 ispell-local-dictionary: "en"
 End:
