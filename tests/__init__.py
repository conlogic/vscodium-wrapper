# ============================================================================
# Title          : Wrapper for VSCodium to use multiple profiles
#
# Classification : Python module
#
# Author         : Dirk Ullrich
#
# Date           : 2021-11-11
#
# Description    : See documentation string below.
# ============================================================================

'''
Init module for the unit test package of package ``vscodium_wrapper``.
'''


# This turns the ``tests`` directory into a Python package.  This eases the
# discovery of tests.


# ----------------------------------------------------------------------------
# Local Variables:
# ispell-local-dictionary: "en"
# End:
