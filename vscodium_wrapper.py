# ============================================================================
# Title          : Wrapper for VSCodium to use multiple profiles
#
# Classification : Python module
#
# Author         : Dirk Ullrich
#
# Date           : 2022-01-03
#
# Description    : See documentation string below.
# ============================================================================

'''
Single module for package ``vscodium_wrapper``.
'''


import pathlib
import subprocess
import shutil
import sys

try:
    from duargparse import DUArgumentParser as ArgumentParser
except ModuleNotFoundError:
    from argparse import ArgumentParser


# Parameters
# ==========

# Version of the very package.
version = '0.3'

# Name for profile base directories.
PROFILE_BASE_DNAME = 'VSCodium-profiles'

# Path of the profile base directories relative to the home directory.
# - User data.
USER_DATA_BASE_RDPATH = pathlib.Path('.config', PROFILE_BASE_DNAME)
# - Extensions.
EXTENSION_BASE_RDPATH = pathlib.Path('.local', 'share', PROFILE_BASE_DNAME)

# Path for the file to store the active profile.
ACT_PROFILE_RFPATH = USER_DATA_BASE_RDPATH / 'active_profile'

# Name of base profile.
BASE_PROFILE_NAME = 'base'

# Names of directories to ignore when reading the user data directory.
IGN_USER_DATA_DNAMES = ['Dictionaries']

# Bundle all relevant configuration parameters.
CONF_PARAMS = {
    'user_data_base': USER_DATA_BASE_RDPATH,
    'extension_base': EXTENSION_BASE_RDPATH,
    'act_profile_file': ACT_PROFILE_RFPATH,
    'base_profile': BASE_PROFILE_NAME,
    'ignore_user_data': IGN_USER_DATA_DNAMES
}


# Functions and classes
# =====================


def main(argv):
    '''
    Main function using the command line argument list `argv`.
    '''
    # Create the commandline parser.
    cmdl_parser = mk_cmdline_parser(argv[0])

    # Parse the commandline.
    (args, other_args) = cmdl_parser.parse_known_args(argv[1:])

    rc = 0

    if args.vc_help:
        run_vscodium(['--help'])
    elif args.vc_version:
        run_vscodium(['--version'])
    else:
        try:
            if args.list_profiles:
                print(list_all_profiles(CONF_PARAMS))
            elif args.set_profile is not None:
                write_active_profile(CONF_PARAMS, args.set_profile)
            elif args.delete_profile:
                delete_profile(CONF_PARAMS, args.delete_profile)
            else:
                run_vscodium(other_args, CONF_PARAMS)
        except VSCodiumError as e:
            print(f"The following error has occurred:\n{e.msg}",
                  file=sys.stderr)
            rc = e.rc if e.rc is not None else 1

    return rc


# For experimentation we create the commandline parser separately.
def mk_cmdline_parser(prog):
    '''
    Create the commandline parser for this very tool called as `prog`.

    :return: the created commandline parser.
    '''
    # Create commandline parser.
    cmdl_parser = ArgumentParser(
        prog=pathlib.Path(prog).name,
        description='''
        Wrapper for VSCodium to use multiple profiles.
        ''')
    # Add option for version.
    cmdl_parser.add_argument(
        '-V', '--version',
        action='version', version=f"%(prog)s {version}"
    )
    # Add option for VSCodium help.
    cmdl_parser.add_argument(
        '--vc-help', help='''
        Show help for VSCodium, and exit.''',
        action='store_true', default=False
    )
    # Add option for VSCodium version.
    cmdl_parser.add_argument(
        '--vc-version', help='''
        Show version of VSCodium, and exit.''',
        action='store_true', default=False
    )
    # Add option to list all VSCodium profiles.
    cmdl_parser.add_argument(
        '-L', '--list-profiles', help='''
        List all profiles and mark the active ones.''',
        action='store_true', default=False
    )
    # Add option to set a VSCodium profile active.
    cmdl_parser.add_argument(
        '-S', '--set-profile', metavar='<profile>', help='''
        Set profile with name <profile> active.''',
        action='store'
    )
    # Add option to remove a VSCodium profile.
    cmdl_parser.add_argument(
        '-D', '--delete-profile', metavar='<profile>', help='''
        Delete profile with name <profile>.  Note that the base profile cannot
        be removed.''',
        action='store'
    )

    return cmdl_parser


def run_vscodium(args, config_params):
    '''
    Run VSCodium with arguments from list `args` with the active profile
    according to dictionary `config_params` of configuration parameters.

    :raise: a :py:exc:`VSCodiumError` if running has failed.
    '''
    # Before running VSCodium, prepare the active profile.
    (data_dpath, exts_dpath) = prep_active_profile(config_params)

    # Run VSCodium for the active profile.
    cmd = ['codium']
    cmd.extend(args)
    cmd.extend(['--user-data-dir', str(data_dpath)])
    cmd.extend(['--extensions-dir', str(exts_dpath)])
    error = None
    try:
        subprocess.run(cmd, check=True)
    except subprocess.CalledProcessError as e:
        msg = 'Error during running VSCodium'
        error = VSCodiumError(msg, rc=e.returncode)
    except OSError as e:
        error = VSCodiumError(msg=e.strerror, rc=e.errno)
    if error is not None:
        raise error


def list_all_profiles(config_params):
    '''
    Generate a string to list all available VSCodium profiles according to the
    dictionary `config_params` of configuration parameters, with the active
    profile marked.

    :return: the generated listing string.

    :raise: a :py:exc:`VSCodiumError` if calculation of either all profiles or
            the active one has failed
    '''
    act_profile = read_active_profile(config_params, fallback=True)
    act_profile_ex = False
    profiles_str = ''
    for p in calc_all_profiles(config_params):
        profiles_str += f"\n{p}"
        if p == act_profile:
            profiles_str += ' *'
            act_profile_ex = True
    if not act_profile_ex:
        profiles_str += f"\n[{act_profile}] *"

    return profiles_str


def delete_profile(config_params, profile):
    '''
    Delete VSCodium profile with name `profile`, according to the dictionary
    `config_params` of configuration parameters, and make the base profile the
    active one.

    Note that the base profile cannot be deleted.

    :raise: a :py:exc:`VSCodiumError` if <profile> names the base profile, or
            if deletion of the profile for <profile> has failed.
    '''
    base_profile = calc_base_profile(config_params)
    if profile == base_profile:
        raise VSCodiumError('Cannot delete the base profile.')

    all_profiles = calc_all_profiles(config_params)
    if profile not in all_profiles:
        raise VSCodiumError(f"Profile '{profile}' does not exist.")

    data_dpath = calc_user_data_dir(config_params)
    profile_data_dpath = data_dpath / profile
    exts_dpath = calc_extensions_dir(config_params)
    profile_exts_dpath = exts_dpath / profile
    try:
        shutil.rmtree(profile_data_dpath)
        shutil.rmtree(profile_exts_dpath)
    except OSError as e:
        raise VSCodiumError(msg=e.strerror, rc=e.errno)

    write_active_profile(config_params, base_profile)


def prep_active_profile(config_params):
    '''
    Prepare the active VSCodium profile such that it can be used to run
    VSCodium for it, according to the dictionary `config_params` of
    configuration parameters.

    :return: pair of paths for the user data directory and extensions
             directory used for the active profile.

    :raise: a :py:exc:`VSCodiumError` if the active profile could not prepared
            to run it.
    '''
    data_dpath = calc_user_data_dir(config_params)
    exts_dpath = calc_extensions_dir(config_params)
    all_profiles = calc_all_profiles(config_params)
    act_profile = read_active_profile(config_params, fallback=True)
    act_profile_data_dpath = data_dpath / act_profile
    act_profile_exts_dpath = exts_dpath / act_profile
    base_profile = calc_base_profile(config_params)
    if act_profile in all_profiles or act_profile == base_profile:
        # The active profile already exists or is the base one -> nothing
        # needs to be prepared.
        pass
    else:
        # Otherwise we copy the base profile.
        try:
            base_profile_data_dpath = data_dpath / base_profile
            shutil.copytree(
                base_profile_data_dpath, act_profile_data_dpath)
            base_profile_exts_dpath = exts_dpath / base_profile
            shutil.copytree(
                base_profile_exts_dpath, act_profile_exts_dpath)
        except OSError as e:
            raise VSCodiumError(msg=e.strerror, rc=e.errno)

    return (act_profile_data_dpath, act_profile_exts_dpath)


def calc_all_profiles(config_params):
    '''
    Calculate all available VSCodium profiles according to the dictionary
    `config_params` of configuration parameters.

    :return: the set of all available profiles.

    :raise: a :py:exc:`VSCodiumError` if calculation has failed or the
            profiles are inconsistent.
    '''
    data_dpath = calc_user_data_dir(config_params)
    if not data_dpath.exists():
        data_profiles = set([])
    elif data_dpath.is_dir():
        raw_data_profiles = {
            d.name for d in data_dpath.iterdir() if d.is_dir()}
        # The user data profile data must be filtered.
        ign_profiles = config_params.get('ignore_user_data', [])
        data_profiles = {
            n for n in raw_data_profiles if n not in ign_profiles}
    else:
        raise VSCodiumError(f"Existing non-directory at\n  {data_dpath}")

    exts_dpath = calc_extensions_dir(config_params)
    if not exts_dpath.exists():
        exts_profiles = set([])
    elif exts_dpath.is_dir():
        exts_profiles = {
            d.name for d in exts_dpath.iterdir() if d.is_dir()}
    else:
        raise VSCodiumError(f"Existing non-directory at\n  {exts_dpath}")

    if not data_profiles == exts_profiles:
        msg = 'Profiles are inconsistent:'
        msg += f"\n+ User data : {data_profiles}"
        msg += f"\n+ Extensions: {exts_profiles}"
        raise VSCodiumError(msg)

    return data_profiles


def read_active_profile(config_params, fallback=False):
    '''
    Read name of the active VSCodium profile from the respective file
    according to the dictionary `config_params` of configuration parameters.

    For a ``True`` switch `fallback` the base profile is used as fallback if
    the active profile cannot be read from the respective file.

    :return: the name of the active profile.

    :raise: a :py:exc:`VSCodiumError` if the respective home directory is not
            a directory, of if the name of the active profile cannot be
            determined.
    '''
    act_profile_fpath = calc_active_profile_file(config_params)
    if act_profile_fpath.is_file():
        with open(act_profile_fpath, 'rt') as act_profile_fobj:
            act_profile = act_profile_fobj.readline().strip()
        if act_profile == '':
            act_profile = None
    else:
        act_profile = None

    if act_profile is None:
        if fallback:
            act_profile = calc_base_profile(config_params)
        else:
            raise VSCodiumError('Cannot determine active profile.')

    return act_profile


def write_active_profile(config_params, profile=None):
    '''
    Ensure that `profile` as name of the active VSCodium profile is written to
    the respective file according to the dictionary `config_params` of
    configuration parameters.

    For a ``None`` value of `profile` the base profile is used.

    :return: ``True`` if the file storing the active profile name was written,
             and ``False`` if this file do not need to be touched.

    :raise: a :py:exc:`VSCodiumError` could not be updated.
    '''
    if profile is None:
        profile = calc_base_profile(config_params)

    act_profile = read_active_profile(config_params, fallback=True)

    # Check whether the file to store the active profile name has to be
    # written.
    act_profile_fpath = calc_active_profile_file(config_params)
    if not act_profile_fpath.is_file():
        write_act_profile = True
    elif not act_profile == profile:
        write_act_profile = True
    else:
        write_act_profile = False

    # Write the file to store the active profile name, if needed.
    if write_act_profile:
        act_profile_dpath = act_profile_fpath.parent
        act_profile_dpath.mkdir(parents=True, exist_ok=True)
        with open(act_profile_fpath, 'wt') as act_profile_fobj:
            act_profile_fobj.write(f"{profile}\n")

    return write_act_profile


def calc_active_profile_file(config_params):
    '''
    Calculate the path for the file to store the active VSCodium profile
    according to the dictionary `config_params` of configuration parameters.

    :return: the path for the file to store the active profile.

    :raise: a :py:exc:`VSCodiumError` if the respective home directory is not
            a directory, or if the path for the file to store the active
            profile cannot be calculated.
    '''
    home_dpath = calc_home_dir(config_params)
    act_profile_fname = config_params.get('act_profile_file')
    if act_profile_fname is None:
        msg = "No parameter 'act_profile_file' in used configuration."
        raise VSCodiumError(msg)
    act_profile_fpath = home_dpath / act_profile_fname

    return act_profile_fpath


def calc_base_profile(config_params):
    '''
    Calculate the name of the base profile according to the dictionary
    `config_params` of configuration parameters.

    :return: name of the base profile.

    :raise: a :py:exc:`VSCodiumError` if the base profile cannot be
            calculated.
    '''
    base_profile = config_params.get('base_profile')
    if base_profile is None:
        raise VSCodiumError(
            "No parameter 'base_profile' in used configuration.")

    return base_profile


def calc_user_data_dir(config_params):
    '''
    Calculate the path of the VSCodium user data directory according to the
    dictionary `config_params` of configuration parameters.

    :return: the calculated user data directory path.

    :raise: a :py:exc:`VSCodiumError` if the user data directory path cannot
            be calculated.
    '''
    home_dpath = calc_home_dir(config_params)
    data_rdpath = config_params.get('user_data_base')
    if data_rdpath is None:
        msg = "No parameter 'user_data_base' in used configuration."
        raise VSCodiumError(msg)
    data_dpath = home_dpath / data_rdpath

    return data_dpath


def calc_extensions_dir(config_params):
    '''
    Calculate the path of the VSCodium extensions directory according to the
    dictionary `config_params` of configuration parameters.

    :return: the calculated extensions directory path.

    :raise: a :py:exc:`VSCodiumError` if the user data directory path cannot
            be calculated.
    '''
    home_dpath = calc_home_dir(config_params)
    exts_rdpath = config_params.get('extension_base')
    if exts_rdpath is None:
        msg = "No parameter 'extension_base' in used configuration."
        raise VSCodiumError(msg)
    exts_dpath = home_dpath / exts_rdpath

    return exts_dpath


def calc_home_dir(config_params):
    '''
    Calculate the path of the used home directory according to the dictionary
    `config_params` of configuration parameters.

    :return: then calculated home directory path.

    :raise: a :py:exc:`VSCodiumError` if the respective home directory is not
            a directory.
    '''
    home_dpath = config_params.get('home_dir')
    if home_dpath is None:
        home_dpath = pathlib.Path.home()
    else:
        home_dpath = pathlib.Path(home_dpath)

    if not home_dpath.is_dir():
        raise VSCodiumError(f"No directory with path\n  {home_dpath}")

    return home_dpath


class VSCodiumError(Exception):
    '''
    Specific exception for errors related to this very script.  Such an
    exception has a message `msg`, and an optional return code `rc`.
    '''
    def __init__(self, msg, rc=None):
        self.msg = msg
        self.rc = rc


    def __str__(self):
        # Since we want to have full control whether the printable string for
        # the exception ends with a ``'.'`` or not we ensure first that there
        # is no ``'.'`` at the end.
        if self.msg.endswith('.'):
            ex_str = self.msg[:-1]
        else:
            ex_str = self.msg

        return ex_str


    def __repr__(self):
        this_name = self.__class__.__name__
        if self.rc is None:
            ex_repr = f"{this_name}({self.msg})"
        else:
            ex_repr = f"{this_name}({self.msg}, rc={self.rc})"

        return ex_repr


# ----------------------------------------------------------------------------
# Local Variables:
# ispell-local-dictionary: "en"
# End:

#  LocalWords:  VSCodium
