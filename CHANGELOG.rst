..
 =============================================================================
 Title          : Wrapper for VSCodium to use multiple profiles

 Classification : reST text file

 Author         : Dirk Ullrich

 Date           : 2022-01-03

 Description    : Change log for the package.
 =============================================================================


=========
Changelog
=========


Version 0.3 (2022-01-03)
========================

- Use non-standard XDG-subdirectories to store the profile stuff.


Version 0.2.1 (2021-11-16)
==========================

- Fix some documentation string glitches (leftovers from an older package
  structure).


Version 0.2 (2021-11-16)
========================

- Fix typos and omissions in some function doc strings.

- Reorder the tests for functions such that the order of their definitions is
  matched as much as possible.

- Factor out the calculation of the base profile name.

- Add option ``-D`` / ``--delete-profile`` to delete a (non-base) VSCodium
  profile.


Version 0.1 (2021-11-16)
========================

Initial version.  Test coverage should be improved, and the documentation is a
bit sketchy yet.


..
 -----------------------------------------------------------------------------
 Local Variables:
 mode: rst
 ispell-local-dictionary: "en"
 End:
