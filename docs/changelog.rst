..
 =============================================================================
 Title          : Wrapper for VSCodium to use multiple profiles

 Classification : reST text file

 Author         : Dirk Ullrich

 Date           : 2021-11-11

 Description    : Change log for the package.
 =============================================================================


.. include:: ../CHANGELOG.rst


..
 -----------------------------------------------------------------------------
 Local Variables:
 mode: rst
 ispell-local-dictionary: "en"
 End:
